from django.contrib import admin
from .models import AutomobileVOService, ServiceAppointment, Technician

# Register your models here.

@admin.register(AutomobileVOService)
class AutomobileVOServiceAdmin(admin.ModelAdmin):
  pass

@admin.register(ServiceAppointment)
class ServiceAppointmentAdmin(admin.ModelAdmin):
  pass

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
  pass