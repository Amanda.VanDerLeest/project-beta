from urllib import response
from django.shortcuts import render
from django.http import JsonResponse
from datetime import datetime

from django.views.decorators.http import require_http_methods
import json
from .encoders import (
  ServiceAppointmentEncoder,
  TechnicianEncoder,
  )
from .models import (
  ServiceAppointment,
  Technician,
  AutomobileVOService
)
# Create your views here.
class VinNotValid(Exception):
  pass

@require_http_methods(["GET"])
def api_pending_service_appointments(request):
  appointments = ServiceAppointment.objects.filter(status="Pending")
  return JsonResponse(
    {"appointments": appointments},
    encoder=ServiceAppointmentEncoder,
  )

@require_http_methods(["GET", "POST"])
def api_service_appointments(request):
  if request.method == "GET":
    appointments = ServiceAppointment.objects.all()
    return JsonResponse(
      {"appointments": appointments},
      encoder=ServiceAppointmentEncoder,
    )
  else:
    try:
      content = json.loads(request.body)
      employee_number = content["technician"]
      try:
        technician = Technician.objects.get(employee_number=employee_number)
        content["technician"] = technician
      except Technician.DoesNotExist:
          response = JsonResponse({"message": "Technician does not exist"})
          response.status_code = 404
          return response

      # Check in the inventory and make sure the VIN entered into a service appointment
      # is not available in our inventory. Otherwise, throw a VinNotValid error and
      # return an error message with invalid VIN.
      try:
        vin = content['vin']
        automobile = AutomobileVOService.objects.get(vin=vin)
        if automobile:
          sale_status = automobile.sale_status
          if sale_status == "Available":
            response = JsonResponse(
              {"message": 'VIN number is available in our inventory. Cannot belong to service customer.'}
            )
            response.status_code = 400
            return response
      except:
        pass

      service_appointment = ServiceAppointment.objects.create(**content)
      print(service_appointment)
      return JsonResponse(
        service_appointment,
        encoder=ServiceAppointmentEncoder,
        safe=False,
      )
    except:
      response = JsonResponse(
        {"message": "Could not create the appointment."}
      )
      response.status_code = 400
      return response

@require_http_methods(["GET"])
def api_service_appointments_vin(request, vin):
    service_appointments = ServiceAppointment.objects.filter(vin=vin)
    if service_appointments.exists():
      return JsonResponse(
        {"appointments": service_appointments},
        encoder=ServiceAppointmentEncoder,
        safe=False
      )
    else:
      response = JsonResponse(
        {"message": 'No service appointments matching that VIN.'}
      )
      response.status_code = 400
      return response


@require_http_methods(["PUT", "DELETE"])
def api_service_appointment(request, id):
  if request.method == "DELETE":
    try:
     count, _ = ServiceAppointment.objects.get(id=id).delete()
     return JsonResponse(
        {"deleted": count > 0}
     )
    except ServiceAppointment.DoesNotExist:
      return JsonResponse({"message": "Service Appointment does not exists"})
  else: #PUT
      content = json.loads(request.body)
      service_appointment = ServiceAppointment.objects.get(id=id)
      try:
        if "technician" in content:
          technician = Technician.objects.get(employee_number=content["technician"])
          content["technician"] = technician
      except Technician.DoesNotExist:
        return JsonResponse(
          {"message": "Invalid technician employee ID."}
        )
      ServiceAppointment.objects.filter(id=id).update(**content)
      service_appointment = ServiceAppointment.objects.get(id=id)
      return JsonResponse(
        service_appointment,
        encoder=ServiceAppointmentEncoder,
        safe=False
      )


@require_http_methods(["GET", "POST"])
def api_technicians(request):
  if request.method == "GET":
    technicians = Technician.objects.all()
    return JsonResponse(
      {"technicians": technicians},
      encoder=TechnicianEncoder,
    )
  else:
    try:
      content = json.loads(request.body)
      technician = Technician.objects.update_or_create(**content)
      print(technician)
      return JsonResponse(
        technician,
        encoder=TechnicianEncoder,
        safe=False,
      )
    except:
      response = JsonResponse(
        {"message": "Could not create the technician. Try using a unique employee number."}
      )
      response.status_code = 400
      return response

@require_http_methods(["GET", "DELETE"])
def api_technician(request, employee_number):
  if request.method == "GET":
    try:
      technician = Technician.objects.get(employee_number=employee_number)
      return JsonResponse(
        technician,
        encoder=TechnicianEncoder,
        safe=False,
      )
    except Technician.DoesNotExist:
      response = JsonResponse({"message": "Technician does not exist"})
      response.status_code = 404
      return response
  else:
    try:
      count, _ = Technician.objects.get(employee_number=employee_number).delete()
      return JsonResponse(
        {"deleted": count > 0}
      )
    except Technician.DoesNotExist:
      return JsonResponse({"message": "Technician does not exists"})