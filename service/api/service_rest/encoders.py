from common.json import ModelEncoder


from .models import AutomobileVOService, ServiceAppointment, Technician

class AutomobileVOEncoder(ModelEncoder):
  model = AutomobileVOService
  properties = ["vin", "import_href", "sale_status"]

class TechnicianEncoder(ModelEncoder):
  model = Technician
  properties = ["name", "employee_number", "id"]

class ServiceAppointmentEncoder(ModelEncoder):
  model = ServiceAppointment
  properties = [
    "id",
    "vin",
    "customer_name",
    "technician",
    "appointment_date_time",
    "reason",
    "status",
  ]

  encoders = {
    "technician": TechnicianEncoder(),
  }

  def get_extra_data(self, object):
    try:
      automobile = AutomobileVOService.objects.get(vin=object.vin)
      sale_status = automobile.sale_status
      if automobile and sale_status == 'Sold':
        return {"was_purchased_at_dealer": True}
      else:
        {"was_purchased_at_dealer": False}
    except:
      pass
    return {"was_purchased_at_dealer": False}






