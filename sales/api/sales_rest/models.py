from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sale_status = models.CharField(max_length=20, null=True)


class Employee(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)


class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    customer_address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length = 20, unique=True)


class Sale(models.Model):
    price = models.CharField(max_length = 20)

    vin = models.ForeignKey(
        AutomobileVO,
        related_name = "sales",
        on_delete = models.PROTECT,
    )

    sale_employee = models.ForeignKey(
        Employee,
        related_name = "sales",
        on_delete = models.PROTECT,
        null = True,
        blank = True,
    )

    purchaser = models.ForeignKey(
        Customer,
        related_name = "sales",
        on_delete = models.PROTECT,
        null = True,
        blank = True,
    )

