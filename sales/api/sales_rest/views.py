from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Employee, Customer, Sale
import json
from django.views.decorators.http import require_http_methods


# Dev Note: ForeignKey VO Detail Encoder:
class AutomobileDetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "color", "year", "sale_status", "import_href"]



class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["customer_name", "phone_number", "customer_address", "id"]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["customer_name", "customer_address", "phone_number"]


class EmployeeListEncoder(ModelEncoder):
    model = Employee
    properties = ["name", "employee_number", "id"]

class EmployeeDetailEncoder(ModelEncoder):
    model = Employee
    properties = ["name", "employee_number"]


class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "sale_employee", "purchaser", "vin", "id"]
    encoders = {
        "sale_employee" : EmployeeListEncoder(),
        "purchaser" : CustomerDetailEncoder(),
        "vin": AutomobileDetailEncoder(),
    }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "sale_employee", "purchaser", "vin", "id"]
    encoders = {
        "sale_employee" : EmployeeListEncoder(),
        "purchaser" : CustomerDetailEncoder(),
        "vin": AutomobileDetailEncoder(),
    }




@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder = SalesListEncoder,
        )
    else: 
        content = json.loads(request.body)
        try:
            employee_number = content["sale_employee"]
            employee = Employee.objects.get(employee_number = employee_number)
            content["sale_employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee number"},
                status=400,
            )
        try:
            purchaser_phone_number = content["purchaser"]
            purchaser = Customer.objects.get(phone_number = purchaser_phone_number)
            content["purchaser"] = purchaser
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid purchaser number"},
                status=400,
            )

        try:
            fetch_vin = content["vin"]
            automobile = AutomobileVO.objects.get(vin = fetch_vin)
            content["vin"] = automobile
            sale_status = automobile.sale_status
            if sale_status == 'Sold': 
                return JsonResponse(
                    {"message": "This VIN has already been used in a sale."}
                )  
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not create the sale"}
            )
            response.status_code = 400
            return response

        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder = SalesListEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse (
            sale,
            encoder = SaleDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse (
                sale,
                encoder = SaleDetailEncoder,
                safe = False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"},
                status = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=pk)
            props = ["price"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder = SaleDetailEncoder,
                safe = False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"},
                status = 404,
            )


@require_http_methods (["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse (
            {"customer": customer},
            encoder = CustomerListEncoder,
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerDetailEncoder,
            safe = False,
        )


@require_http_methods (["GET", "DELETE", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder = CustomerDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder = CustomerDetailEncoder,
                safe = False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["customer_name", "customer_address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder = CustomerDetailEncoder,
                safe = False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status = 404,
            )


@require_http_methods (["GET", "POST"])
def api_list_employees(request):
    if request.method == "GET":
        employee = Employee.objects.all()
        return JsonResponse(
            {"employee": employee},
            encoder = EmployeeListEncoder,
        )
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder = EmployeeListEncoder,
            safe = False,
        )

@require_http_methods (["GET", "DELETE", "PUT"])
def api_show_employee(request, pk):
    if request.method == "GET":
        employee = Employee.objects.get(id=pk)
        return JsonResponse(
            employee,
            encoder = EmployeeDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        try:
            employee = Employee.objects.get(id=pk)
            employee.delete()
            return JsonResponse(
                employee,
                encoder = EmployeeDetailEncoder,
                safe = False,
            )
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            employee = Employee.objects.get(id=pk)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(employee, prop, content[prop])
            employee.save()
            return JsonResponse(
                employee,
                encoder = EmployeeDetailEncoder,
                safe = False,
            )
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status = 404
            )
