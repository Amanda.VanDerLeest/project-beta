import React, { useEffect, useState } from 'react'
import ServiceAppointmentTable from './ServiceAppointmentTable'



class ServiceHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      appointments: [],
      noAppointments: false
    };
    this.handleVinChange = this.handleVinChange.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value })
  }

  async handleSearch(event) {
    event.preventDefault();
    const url = `http://localhost:8080/api/service_appointments/vin/${this.state.vin}/`;
    const fetchConfig = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      this.setState({
        appointments: data.appointments,
        noAppointments: false,
      })
    } else {
      this.setState({
        appointments: [],
        noAppointments: true
      })
    }
  }

  render() {

    let messageClasses = 'alert alert-warning d-none mb-0';
    let tableClasses = '';
    if (this.state.noAppointments) {
      messageClasses = 'alert alert-warning mb-0';
      tableClasses = 'd-none';
    }
    return (
      <>
        <h1>Search Service History</h1>
        <div className="row justify-content-center mt-3">
          <form onSubmit={this.handleSearch} className="col-6">
            <div className="input-group mb-3 mt-3">
              <input
                onChange={this.handleVinChange}
                value={this.state.vin}
                type="text"
                className="form-control"
                placeholder="VIN"
                aria-label="search"
                aria-describedby="basic-addon2"
              />
              <div className="input-group-append">
                <button className="btn btn-outline-secondary" type="submit">
                  Search VIN
                </button>
              </div>
            </div>
          </form>
          <ServiceAppointmentTable
            appointments={this.state.appointments}
            hideManage={true}
            hideStatus={false}
          ></ServiceAppointmentTable>
        </div>
        <div className={messageClasses} id="warning-message">
          Sorry. There are no appointments for that VIN.
        </div>
      </>
    )
  }



}
export default ServiceHistory
