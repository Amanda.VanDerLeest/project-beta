import ServiceAppointmentRow from "./ServiceAppointmentRow"

export default function ServiceAppointmentTable(props) {
  const {
    appointments,
    hideManage,
    hideStatus
  } = props

  return (
    <>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Vin</th>
            <th scope="col">Customer Name</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col">Purchased Here</th>
            {!hideStatus && <th scope="col">Appt. Status</th>}
            {!hideManage && <th scope="col">Manage</th>}
          </tr>
        </thead>
        <tbody>
          {appointments.map(appt =>
            <ServiceAppointmentRow
              key={appt.id}
              id={appt.id}
              customer_name={appt.customer_name}
              vin={appt.vin}
              was_purchased_at_dealer={appt.was_purchased_at_dealer}
              date={appt.appointment_date_time}
              technician={appt.technician.name}
              reason={appt.reason}
              status={appt.status}
              hideManage={hideManage}
              hideStatus={hideStatus}
            />
          )}
        </tbody>
      </table>
    </>
  )
}