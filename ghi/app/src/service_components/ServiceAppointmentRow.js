import React, { useState, useEffect } from 'react'
// import { ArrowRight, StarFill } from 'react-bootstrap-icons';

function ServiceAppointmentRow(props) {
  const {
    id,
    vin,
    customer_name,
    was_purchased_at_dealer,
    date,
    technician,
    reason,
    status,
    hideManage,
    hideStatus
  } = props

  const [didClickContinueButton, setDidClickContinueButton] = useState(false)
  const [didClickCancelButton, setDidClickCancelButton] = useState(false)
  const [apiEndpoint, setApiEndpoint] = useState([])
  const BASE_URL = `http://localhost:8080/api/service_appointments`;


  function handleClickOnCompleted(e) {
    setDidClickContinueButton(true)
    setApiEndpoint(`${BASE_URL}/${id}/`)
  }

  function handleClickOnCancel(e) {
    setDidClickCancelButton(true)
    setApiEndpoint(`${BASE_URL}/${id}/`)
  }

  async function postData(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    return response.json();
  }

  useEffect(() => {
    if (didClickContinueButton === true) {
      postData(apiEndpoint, { "status": "Completed" })
        .then(data => {
          console.log(data);
        });
      setDidClickContinueButton(false)
      window.location.reload();
    }
  }, [didClickContinueButton, apiEndpoint])

  useEffect(() => {
    if (didClickCancelButton === true) {
      postData(apiEndpoint, { "status": "Cancelled" })
        .then(data => {
          console.log(data);
        });
      setDidClickContinueButton(false)
      window.location.reload();
    }
  }, [didClickCancelButton, apiEndpoint])


  var purchased_here = ''

  if (was_purchased_at_dealer) {
    // purchased_here = <StarFill color="gold" />
    purchased_here = "Yes!"
  } else {
    purchased_here = "Nope."
  }

  var parsedDate = new Date(date)
  var day_month_year = parsedDate.toDateString()
  var parsedTime = parsedDate.toLocaleTimeString()

  return (
    <>
      <tr>
        <td>{vin}</td>
        <td>{customer_name}</td>
        <td>{day_month_year}</td>
        <td>{parsedTime}</td>
        <td>{technician}</td>
        <td>{reason}</td>
        <td>{purchased_here}</td>
        {!hideStatus && <td>{status}</td>}
        <td>
          {!hideManage && <div>
            <button onClick={handleClickOnCancel} type="button" className="btn btn-outline-danger btn-sm">Cancel</button>
            <button onClick={handleClickOnCompleted} type="button" className="btn btn-outline-success btn-sm">Completed</button>
          </div>}
        </td>
      </tr>
    </>
  )
}

export default ServiceAppointmentRow