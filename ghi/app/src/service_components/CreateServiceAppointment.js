import React from 'react';
// import { Redirect } from 'react-router'



class CreateServiceAppointment extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      vin: '',
      customer_name: '',
      technician: '',
      technicians: [],
      reason: '',
      time: '',
      date: '',
      createdAppointment: false,
      noAppointmentCreated: false,
    }
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleTechnicianSelection = this.handleTechnicianSelection.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ customer_name: value });
  }

  handleTechnicianSelection(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;

    this.setState({ reason: value });
  }

  handleTimeChange(event) {
    const value = event.target.value;
    this.setState({ time: value });
  }

  handleDateChange(event) {
    const value = event.target.value;
    this.setState({ date: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians
    const appointment_date_time = `${data.date}T${data.time}:00+00:00`
    delete data.date
    delete data.time
    delete data.createdAppointment
    delete data.noAppointmentCreated
    data['appointment_date_time'] = appointment_date_time

    const serviceApptPostUrl = 'http://localhost:8080/api/service_appointments/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(serviceApptPostUrl, fetchConfig)
    if (response.ok) {
      const cleared = {
        vin: '',
        customer_name: '',
        technician: '',
        reason: '',
        time: '',
        date: '',
        createdAppointment: true,
        noAppointmentCreated: false,
      };
      this.setState(cleared);
    } else {
      const cleared = {
        vin: '',
        customer_name: '',
        technician: '',
        reason: '',
        time: '',
        date: '',
        createdAppointment: false,
        noAppointmentCreated: true,
      };
      this.setState(cleared)
    }

  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians })
    }
  }

  render() {

    let successMessageClasses = 'alert alert-success d-none mb-0';
    let warningMessageClasses = 'alert alert-warning d-none mb-0'
    let formClasses = '';
    if (this.state.createdAppointment) {
      successMessageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }
    if (this.state.noAppointmentCreated) {
      warningMessageClasses = 'alert alert-warning mb-0'
      formClasses = 'd-none';
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Service Appointment</h1>
            <form className={formClasses} onSubmit={this.handleSubmit} id="create-service-appointment-form" >
              <div className="form-floating mb-3">
                <input onChange={this.handleVinChange} value={this.state.vin} placeholder="Vehicle Identification Number (VIN)" required type="text" name="vin" id="vin" className="form-control" />
                <div id="vinHelp" className="form-text">You cannot enter a VIN of a car that is currently available for sale in our inventory.</div>
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="cust_name" id="cust_name" className="form-control" />
                <label htmlFor="cust_name">Customer name</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleTechnicianSelection} value={this.state.technician} required name="technician" id="technician" className="form-select">
                  <option value="hi">Choose a technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                      <option key={technician.employee_number} value={technician.employee_number}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleDateChange} value={this.state.date} placeholder="" required type="date" name="date" id="date" className="form-control" />
                <label htmlFor="date">Appointment Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTimeChange} value={this.state.time} placeholder="" required type="time" name="time" id="time" className="form-control" />
                <label htmlFor="time">Appointment Time</label>
              </div>
              <button className="btn btn-primary">Create Service Appointment</button>
            </form>
            <div className={successMessageClasses} id="success-message">
              You've added a new service appointment!
            </div>
            <div className={warningMessageClasses} id="warning-message">
              Uh oh! That didn't work! Please try again. You're probably using a VIN that is marked as 'available' in our inventory. Please correct that!
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CreateServiceAppointment