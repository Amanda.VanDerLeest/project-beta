import React, { useEffect, useState } from 'react'
import ServiceAppointmentTable from './ServiceAppointmentTable'



export default function ServiceAppointmentsList() {
  const [salesAppointments, setSalesAppointments] = useState([])
  const [loading, setLoading] = useState(true)
  const BASE_URL = 'http://localhost:8080/api/service_appointments/pending/'

  useEffect(() => {
    setLoading(true)
    fetch(BASE_URL)
      .then(response => response.json())
      .then(data => {
        setSalesAppointments(data.appointments)
        setLoading(false)
      })
  }, [])


  if (loading) return (
    <div className="px-4 py-5 my-5 text-center">
      <div className="d-flex spinner-grow" role="status">
        <span className="sr-only"></span>
      </div>
    </div>
  )

  return (
    <>
      <h1>Pending Service Appointments</h1>
      {/* <button onClick={handleClickOnRefresh} className='btn btn-success pull-right'>Refresh</button> */}
      <ServiceAppointmentTable
        appointments={salesAppointments}
        hideStatus={true}
        hideManage={false}
      >
      </ServiceAppointmentTable>
    </>
  )
}