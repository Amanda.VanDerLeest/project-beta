import React, { useEffect, useState } from 'react'
import ServiceButton from './ServiceButton'

export default function ServiceButtonPage() {

  const linkProperties = [
    { "link": 'appointments', "buttonText": 'List of Pending Service Appointments' },
    { "link": 'create_technician', "buttonText": 'Add a Technician' },
    { "link": 'create_service_appt', "buttonText": 'Add a Service Appointment' },
    { "link": 'history', "buttonText": 'Service Appointment History' },
  ]

  return (
    <>
      <div className="px-3 py-3 my-2 text-center">
        <h1 className="display-5 fw-bold">Services</h1>
      </div>
      <div className="btn d-grid gap-2 col-8 mx-auto">
        {linkProperties.map(link =>
          <ServiceButton
            key={link["link"]}
            link={link["link"]}
            buttonText={link["buttonText"]}
          />
        )}
      </div>
    </>
  )
}