import React from "react";
import { Link } from 'react-router-dom';


function SaleHistory(props) {
    return(
        <tr>
            <td>{props.sale.sale_employee.name}</td>
            <td>{props.sale.sale_employee.employee_number}</td>
            <td>{props.sale.purchaser.customer_name}</td>
            <td>{props.sale.vin.vin}</td>
            <td>{props.sale.price}</td>
        </tr>
    )
}


class EmployeeSaleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sale_employee: '',
      sale_employees: [],
      SaleColumn: [],
      allSales: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeSaleEmployee = this.handleChangeSaleEmployee.bind(this);
  }

  async componentDidMount() {
    
    const url = 'http://localhost:8090/api/sales/';
    // personal note: this URL is for getting data for the list of sales 

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({SaleColumn: data.sales, allSales: data.sales})
      }
    } catch (e) {
      console.error(e);
    }


const saleEmployeesUrl = 'http://localhost:8090/api/employee/';

        const responseEmployee = await fetch(saleEmployeesUrl);

        if(responseEmployee.ok) {
            const data = await responseEmployee.json();
            this.setState({sale_employees: data.employee});
        }

  
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.employees;
  }

  handleChangeSaleEmployee(event) {
    const value = event.target.value;
    this.setState({sale_employee: value});
    const filteredSales = this.state.allSales.filter(sale => sale.sale_employee.employee_number == value)
    this.setState ({SaleColumn: filteredSales})
  }




  render() {
    return (
      <>
        <div>
          <h1 className="display-5 fw-bold text-center">Employee's Sale History</h1>
            <form onSubmit={this.handleSumbit}>
                <div className="mb-3">
                  <select onChange={this.handleChangeSaleEmployee} required name="sale_employee" id="sale_employee" className="form-select">
                      <option value="">Choose the Sale Employee</option>
                      {this.state.sale_employees.map(sale_employee => {
                          return (
                              <option key={sale_employee.id} value={sale_employee.employee_number}>{sale_employee.name} - {sale_employee.employee_number}</option>
                          )
                      })}
                  </select>
                </div>
            </form>
        </div>

        <table className="table table-dark table-hover">
            <thead>
                <tr>
                    <th>Sales Employee</th>
                    <th>Sales Employee Number</th>
                    <th>Purchaser/Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {this.state.SaleColumn.map((saleList, index) => {
                    return (
                        <SaleHistory key={index} sale={saleList}/>
                    );
                })}
            </tbody>
        </table>
      </>
    );
  }
}


export default EmployeeSaleList;