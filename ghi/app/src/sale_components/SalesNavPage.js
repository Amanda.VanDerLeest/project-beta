import React from "react";
import { Link } from 'react-router-dom';

function SalesNavPage () {
    return (
        <>
            <div className="px-3 py-3 my-2 text-center">
                <h1 className="display-5 fw-bold">Sales</h1>
            </div>
            <div className="btn d-grid gap-2 col-6 mx-auto">
                <Link to="/sales/customer_form" className="btn btn-outline-dark btn-lg px-4 gap-3">Create a Customer</Link>
            </div>
            <div className="btn d-grid gap-2 col-6 mx-auto">
                <Link to="/sales/employee_form" className="btn btn-outline-dark btn-lg px-4 gap-3">Create an Employee</Link>
            </div>
            <div className="btn d-grid gap-2 col-6 mx-auto">
                <Link to="/sales/sale_form" className="btn btn-outline-dark btn-lg px-4 gap-3">Create a Sale</Link>
            </div>
            <div className="btn d-grid gap-2 col-6 mx-auto">
                <Link to="/sales/sale_list" className="btn btn-outline-dark btn-lg px-4 gap-3">List of Sales</Link>
            </div>
            <div className="btn d-grid gap-2 col-6 mx-auto">
                <Link to="/sales/employee_sale_list" className="btn btn-outline-dark btn-lg px-4 gap-3">List of Employee Sales</Link>
            </div>
        </>
    );

}

export default SalesNavPage;