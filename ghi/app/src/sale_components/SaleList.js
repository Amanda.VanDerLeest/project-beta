import React from "react";
import { Link } from 'react-router-dom';



function SaleHistory(props) {
    return(
        <tr>
            <td>{props.sale.sale_employee.name}</td>
            <td>{props.sale.sale_employee.employee_number}</td>
            <td>{props.sale.purchaser.customer_name}</td>
            <td>{props.sale.vin.vin}</td>
            <td>{props.sale.price}</td>
        </tr>
    )
}


class SaleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      SaleColumn: [],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/sales/';
    // personal note: this URL was for getting data for the list of sales 

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({SaleColumn: data.sales})
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div>
          <h1 className="display-5 fw-bold text-center">Sale History</h1>
        </div>

        <table className="table table-dark table-hover">
            <thead>
                <tr>
                    <th>Sales Employee</th>
                    <th>Sales Employee Number</th>
                    <th>Purchaser/Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {this.state.SaleColumn.map((saleList, index) => {
                    return (
                        <SaleHistory key={index} sale={saleList}/>
                    );
                })}
            </tbody>
        </table>
      </>
    );
  }
}


export default SaleList;