import React from "react";
import { Link } from 'react-router-dom';

class ModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            manufacturer_id: '',
            manufacturers: [],
            hasSignedUp: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this)

    }


    async componentDidMount() {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(manufacturerUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers })
        }

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.manufacturers;
        delete data.hasSignedUp;

        const modelUrl = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const newModel = await response.json();
            this.setState({
                name: '',
                picture_url: '',
                manufacturer_id: '',
                manufacturers: [],
                hasSignedUp: true,
            });
        }
    }


    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }

    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer_id: value });
    }



    render() {

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
            messageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
        }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new Model</h1>
                        <form className={formClasses} onSubmit={this.handleSubmit} id="create-model-form">

                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Model Name</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangePictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>


                            <div className="mb-3">
                                <select onChange={this.handleChangeManufacturer} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                    <option value="">Select a Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                        <br />
                        <div className={messageClasses} id="success-message">
                            You've added a new Model!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


// Dev Note: line 30 the first manufacturers is set to whatever variable name I want, but data.manufacturers comes from the data variable name manufacurers

// Dev Note: the reason why line 56 & 57 are different names is because views needs it to be manufacturer_id to be able to post. Line 57
//          we are able to get the array of manufacturers from componant did mount



export default ModelForm;