import React from 'react';


class CreateManufacturerForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      manufacturerCreated: false,
      manufacturerNotCreated: false,
    }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.manufacturerCreated
    delete data.manufacturerNotCreated

    const manufacturerPostUrl = 'http://localhost:8100/api/manufacturers/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(manufacturerPostUrl, fetchConfig)
    if (response.ok) {
      const newManufacturer = await response.json();
      const cleared = {
        name: '',
        manufacturerCreated: true,
        manufacturerNotCreated: false,
      };
      this.setState(cleared)
    } else {
      const cleared = {
        name: '',
        manufacturerCreated: false,
        manufacturerNotCreated: true,
      };
      this.setState(cleared)
    }

  }

  render() {
    let successMessageClasses = 'alert alert-success d-none mb-0';
    let warningMessageClasses = 'alert alert-warning d-none mb-0';
    let formClasses = '';
    if (this.state.manufacturerCreated) {
      successMessageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }
    if (this.state.manufacturerNotCreated) {
      warningMessageClasses = 'alert alert-warning mb-0';
      formClasses = 'd-none';
    }

    return (
      <>
        <div className="row">
          <div className="offset-3 col-7">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Manufacturer</h1>
              <form className={formClasses} id="create-manufacturer-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input onChange={this.handleNameChange} value={this.state.name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Manufacturer Name</label>
                  <div id="manufacturerHelp" className="form-text">Please enter a unique manufacturer.</div>
                </div>
                <button className="btn btn-primary">Create Manufacturer</button>
              </form>
              <div className={successMessageClasses} id="success-message">
                You've added a new manufacturer!
              </div>
              <div className={warningMessageClasses} id="warning-message">
                Uh oh! Something went wrong. Please try adding the manufacturer again.
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default CreateManufacturerForm