import NavButton from '../NavButton'
export default function InventoryNav(props) {

  const linkProperties = [
    { "link": '/inventory/create_manufacturer', "buttonText": 'Create Manufacturer' },
    { "link": '/inventory/list_manufacturers', "buttonText": 'List of Manufacturers' },
    { "link": '/inventory/list_models', "buttonText": 'List of Vehicle Models' },
    { "link": '/inventory/create_model', "buttonText": 'Create Vehicle Model' },
    { "link": '/inventory/list_automobiles', "buttonText": 'List Automobiles' },
    { "link": '/inventory/create_automobile', "buttonText": 'Create Automobile' },
  ]

  return (
    <>
      <div className="px-3 py-3 my-2 text-center">
        <h1 className="display-5 fw-bold">Inventory</h1>
      </div>
      <div className="btn d-grid gap-2 col-8 mx-auto">
        {linkProperties.map(link =>
          <NavButton
            key={link["link"]}
            link={link["link"]}
            buttonText={link["buttonText"]}
          />
        )}
      </div>
    </>
  )
}