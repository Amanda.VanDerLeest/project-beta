import React from "react";
import { Link } from 'react-router-dom';


function AvailableInventory(props) {
    return(
        <tr>
            <td>{props.auto.vin}</td>
            <td>{props.auto.color}</td>
            <td>{props.auto.year}</td>
            <td>{props.auto.model.name}</td>
            <td>{props.auto.model.manufacturer.name}</td>
        </tr>
    )
}


class InventoryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      InventoryColumn: [],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/available/';
    // Dev note: this URL was for getting data for the list of available automobiles 

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({InventoryColumn: data.autos})
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div>
          <h1 className="display-5 fw-bold text-center">Available Inventory</h1>
        </div>

        <table className="table table-dark table-hover">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {this.state.InventoryColumn.map((inventoryList, index) => {
                    return (
                        <AvailableInventory key={index} auto={inventoryList}/>
                    );
                })}
            </tbody>
        </table>
      </>
    );
  }
}

export default InventoryList;