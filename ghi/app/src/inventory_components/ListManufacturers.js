import React from 'react';

function ManufacturerTableRow(props) {
  const {
    manufacturer,
    onDelete
  } = props

  return (
    <>
      <tr>
        <td>{manufacturer.name}</td>
        <td>
          <button
            className="btn btn-outline-danger btn-sm"
            onClick={() => {
              onDelete(manufacturer.id)
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    </>
  )

}

class ManufacturerList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      manufacturers: [],
    }
    this.deleteManufacturer = this.deleteManufacturer.bind(this)
  }

  async deleteManufacturer(id) {
    const url = `http://localhost:8100/api/manufacturers/${id}/`
    const response = await fetch(url, {
      method: "DELETE"
    });
    if (response.ok) {
      window.location.reload();
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers })
    }
  }

  render() {
    return (
      <>
        <h1>Manufacturers</h1>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Manufacturer</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.state.manufacturers.map(manufacturer =>
              <ManufacturerTableRow
                key={manufacturer.id}
                manufacturer={manufacturer}
                onDelete={this.deleteManufacturer}
              />
            )}
          </tbody>
        </table>
      </>
    )
  }
}

export default ManufacturerList