import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import InventoryNav from './inventory_components/InventoryNav'
import CreateManufacturerForm from './inventory_components/CreateManufacturer'
import ManufacturerList from './inventory_components/ListManufacturers'
import VehicleModelList from './inventory_components/ListVehicleModels'
import ModelForm from './inventory_components/ModelForm'
import AutomobileForm from './inventory_components/AutomobileForm'
import InventoryList from './inventory_components/InventoryList'
import ServiceAppointmentsList from './service_components/ServiceAppointmentsList'
import ServiceButtonPage from './service_components/ServiceButtonPage'
import CreateTechnicianForm from './service_components/CreateTechnicianForm'
import CreateServiceAppointment from './service_components/CreateServiceAppointment'
import ServiceHistory from './service_components/ServiceAppointmentsHistory'
import SalesNavPage from './sale_components/SalesNavPage'
import CustomerForm from './sale_components/CustomerForm'
import EmployeeForm from './sale_components/EmployeeForm'
import SaleForm from './sale_components/SaleForm'
import SaleList from './sale_components/SaleList'
import EmployeeSaleList from './sale_components/EmployeeSaleList'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='inventory'>
            <Route path='' element={<InventoryNav />}> </Route>
            <Route path='create_manufacturer' element={<CreateManufacturerForm />}> </Route>
            <Route path='list_manufacturers' element={<ManufacturerList />}> </Route>
            <Route path='list_models' element={<VehicleModelList />}> </Route>
            <Route path='create_model' element={<ModelForm />}> </Route>
            <Route path='list_automobiles' element={<InventoryList />}> </Route>
            <Route path='create_automobile' element={<AutomobileForm />}> </Route>
          </Route>
          <Route path='services'>
            <Route path='' element={<ServiceButtonPage />}> </Route>
            <Route path='appointments' element={<ServiceAppointmentsList />}> </Route>
            <Route path='history' element={<ServiceHistory />}> </Route>
            <Route path='create_technician' element={<CreateTechnicianForm />}> </Route>
            <Route path='create_service_appt' element={<CreateServiceAppointment />}> </Route>
          </Route>
          <Route path='sales'>
            <Route path='' element={<SalesNavPage />}> </Route>
            <Route path='customer_form' element={<CustomerForm />}> </Route>
            <Route path='employee_form' element={<EmployeeForm />}> </Route>
            <Route path='sale_form' element={<SaleForm />}> </Route>
            <Route path='sale_list' element={<SaleList />}> </Route>
            <Route path='employee_sale_list' element={<EmployeeSaleList />}> </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
