# CarCar

Team:

* Jessica Dyer - Service
* Amanda V. - Sales

## Design

## Inventory microservice

In order to track the sale status of each vehicle in inventory, we added a property 'sale_status' to the Automobile model. This is essentially an Enum (Django text.choices) with the options: 'Available' and 'Sold'. When an Automobile is created, this value is set to 'Available'.

This change solved two problems for the team:
- The sales microservice needed a way to:
    - Check to see if the vehicle was already sold when selling a vehicle;
    - Mark the vehicle as 'sold' after creating a sales ticket;
- The service microservice needed a way to:
    - Check to see if a VIN entered into a service ticket was ever in inventory AND marked as sold;
    - If the VIN is in our inventory but marked as 'Available', you cannot create a service ticket for that
        vehicle;

There may be some latency issues with this approach. For example, if a new Automobile is created, it needs to be updated in each microservice (sales and service) through the poller. If the sales microservice marks the Automobile as sold by creating a sale, the Automobile will then be updated as 'Sold', however that information will take some time to come back through the poller to reach each microservice. In that time, the service side might create a service appointment, not rendering that the VIN has been sold yet, and not being able to create a service ticket.

### TLDR;
- There might be a better way to structure the passing of data other than polling due to latency issues.

### Frontend functionality:
- Create manufacturer: Jessica completed this component. Sends a POST request to appropriate endpoint and creates a manufacturer in database.
- List manufacturers: Jessica completed this component. Sends a GET request to the appropriate endpoint and displays all manufacturers on screen.
- List of vehicle models: Jessica completed this component. Sends a GET request to the appropriate endpoint and displays the vehicle models on screen.
- Create Vehicle Models - Amanda V completed this component. Sends a POST request to the appropriate endpoint and creates a Vehicle Model in the database. The Vehicle Model fields are Model Name, Picture Url, and a select tag to Select a Manufacturer.
- List Automobiles  - Amanda V completed this component. Sends a GET request to the appropriate endpoint and displays a list of all the Automobile in the available inventory. The fields that are shown for each available automobile are VIN, Color, Year, Model, and Manufacturer. 
- Create Automobile - Amanda V completed this component. This sends a POST request to the appropriate endpoint and creates an Automobile in the database. The fields are Color, Year, Vin and there is a select tag to choose the Model.




## Service microservice (Jessica Dyer)

### Models:

#### AutomobileVOService
- import_href
- vin
- sale_status
- The AutomobileVOService model is a model that holds data received through the poller from the Inventory microservice. There is no API endpoint for this model.

#### Technician
- name
- employee_number
- API endpoints:
    - List all technicians GET, POST
    - Detail of one technician GET, DELETE

#### ServiceAppointment
- Status (class) ['Pending', 'Cancelled', 'Completed']
- vin
- customer_name
- technician (Technician object)
- reason
- status (Status object)
- API endpoints:
    - List pending service appointments: GET
    - List all service appointments: GET, POST
    - Get service appointment by VIN: GET
    - Detail of service appointment: GET, DELETE

#### Backend functionality:
- When a service ticket is made, we check our inventory for the VIN associated with the car. In the GET process, if the VIN is in our inventory AND marked as sold, we set a variable 'was_purchased_at_dealer' to True.
- If the VIN is in our inventory but marked as 'Available' we assume the VIN is incorrect because the vehicle has not been sold yet.

#### Frontend functionality:
- View a list of pending service appointments;
    - If the 'Cancel' button is clicked, a PUT request is sent to that service appointment API endpoint and the 'status' is updated to 'Cancelled'. The page is reloaded and only pending appointments remain.
    - If the 'Completed' button is clicked, a PUT request is sent to that service appointment API endpoint and the 'status' is updated to 'Completed'. The page is reloaded and only pending appointments remain.
- On submit, the add a technician form sends a POST request to the appropriate API endpoint and adds a technician to the database.
- On submit, the add a service appointment sends a POST request to the appropriate API endpoint and adds a sales appointment to the database. The 'status' is automatically set to 'Pending'. It will now display on the list of pending service appointments.
- Service appointment history: The VIN field is a form. On submit it sends a GET request to the appropriate endpoint which filters for all service appointments for that VIN.


## Sales microservice (Amanda VanDerLeest)

How to set if it has been sold or not? 
- My partner came up with a great idea to add a sale_status to the Automobile Model. In there, she set the default value to a newly created automobile to availble. She then created a function in the Inventory Views to handle filtering out Availble Vehicles by the dafault status being Availble. I was later able to implement this field in my code because of her doing this. I was also able to bypass adding a function to filter out the availble vehicles to show in the list of inventory in the inventory section. I did, however, get to implement that filter function in React when I was showing the list of sales by a specific employee. 


### Models to make:
- Employee Model
- Customer Model
- Sale Ticket Model
    Foreign Keys: Employee & Customer/Purchaser & Vin

### Set up Poller.py to access the vehicle and inventory information to use in the sales microservice 

### Structuring the main Sales page:
- Main Sales Page will be a Nav Page that will contain all the links for the criteria of the project. Links are stated below:

### *ADD SALES PERSON (FORM) - NAV LINK*
Add a Sales/Employee Person:
    - Create a FORM that has:
        -employee name
        -employee number
    - *Create a link in NavBar to get to the Add a sales person form*


### *ADD POTENTIAL CUSTOMER (FORM) - NAV LINK*
Add a potential customer:
    - create a FORM that has:
        - customer Name
        - customer Address
        - customer Phone Number
    - Customer is created in the application


### *ADD A SALE RECORD (FORM) - NAV LINK*
Record a new sale:
    - Sale must be pulled from the current inventory!!
    - Car can not be sold yet (once sold, should be removed from active inventory list)
    - Use a form to create a new sale. Have that sale include:
        - Choose an automobile (Select tag)
        - choose a sales person (select tag)
        - Choose a customer (select tag)
        - sales price (input tag?)
        - (create button)
    - When form is submitted, sale record is stored in the application


### *ALL VEHICLE SALES (LIST) - NAV LINK*
List all vehicle sales:
    - Need a page to show ALL SALES
    - Each sale should have the details:
        - Sales person's name
        - Sales person's employee number
        - Purchaser's name
        - Automobile VIN
        - Price of the sale


### *EMPLOYEES SALE HISTORY (LIST WITH SELECT TAG : detail list?) - NAV LINK*
List of all sales for a sales person's sales hisotry:
    -Need to show a list of sales for a sales person's employee number:
        - Create a page with a select tag that allows someone to choose a sales person
        - When the dropdown changes > fetch all of the sales associated with the sales person selected in the dropdown
            - Each sale should be on a table that shows:
                - Sales Person's Name
                - Customer
                - VIN number for the car
                - Sale Price



### NOTE: Rules:
- Cannot sell a car without it being in the inventory first
- Cannot sell a car that is already sold (needs to have the unique vin_id)
- Cannot have an employee with the same employee id


### Explain integration with inventory:
The sale of a car at this dealership will have to access the inventory information of this dealership. The only way a car can be sold is if it is first available in the inventory. The inventory will be updated and the car will be removed from the inventory if the car has been sold in the Sales section.
